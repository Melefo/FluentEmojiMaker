export default interface Emoji {
	head: number;
	eyes: number;
	mouth: number;
	eyebrows: number;
	details: number;
}
