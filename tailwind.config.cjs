/** @type {import('tailwindcss').Config} */
module.exports = {
	content: ['./src/**/*.{html,js,svelte,ts}'],
	theme: {
		extend: {
			colors: {
				'ms-primary': '#1f1f1f',
				'ms-secondary': '#292929',
				'ms-purple': '#9780e5',
				'ms-purple-tint': '#ac99ea',
				'ms-purple-hover': '#b9aaee',
				'ms-pink': '#e666cc',
				'ms-orange': '#fa97a1',
				'ms-green': '#7edae3'
			},
			boxShadow: {
				ms: '0 0 black, 0 0 black, 0 2px 10px 4px #41414133',
				'ms-hover': '0 0 black, 0 0 black, 0 2px 10px 4px #414141cc'
			}
		}
	},
	plugins: []
};
